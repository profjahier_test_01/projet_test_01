# Application de base pyxel
## Création des 3 fonctions fondamentales : run, update, draw
Le principe général est d'**appeler régulièrement** la fonction `update` qui s'occupe de faire la mise à jour de tous les éléments graphiques de l'application (personnage, ennemis, décors...), puis la fonction `draw` qui s'occupe de dessiner à l'écran tous ces éléments graphiques.
